import React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import Login from './Login'
import Register from './Register'
import HomePage from './HomePage'

const Stack = createStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Login Page" component={Login} />
      <Stack.Screen name="Home Page" component={HomePage} />
      <Stack.Screen name="Register Page" component={Register} />
    </Stack.Navigator>
  );
};

export default HomeStack;
