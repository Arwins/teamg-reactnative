import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

const HomePage = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={{color: '#FFFFFF'}}>Best Genre</Text>
      <Text style={{color: '#FFFFFF'}}>Hot Thriller Movies</Text>
      <TouchableOpacity
        onPress={() => {navigation.goBack()}}>
        <Text style={{color: '#FFFFFF'}}>Go Back</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#343434',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomePage;
