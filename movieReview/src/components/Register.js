import React from 'react';
import {StyleSheet, View, TextInput, Text, TouchableOpacity} from 'react-native';

const Register = ({navigation}) => {
  return (
    <View style={styles.container}>
      <TextInput style={styles.inputText} placeholder={'  Name'}  />
      <TextInput style={styles.inputText} placeholder={'  Username'}  />
      <TextInput style={styles.inputText} placeholder={'  Email'}  />
      <TextInput style={styles.inputText} placeholder={'  Password'} />
      <TouchableOpacity style={styles.button} onPress={() => {navigation.push('Home Page')}} >
        <Text style={styles.buttonText}>SIGN UP</Text>
      </TouchableOpacity>
      <View style={styles.bawah}>
        <Text style={{color: '#FFFFFF'}}>Already have an account?</Text>
        <Text style={{color: 'red'}} onPress={() => navigation.push('Login Page')}> Sign In</Text>
      </View>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#343434',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputText: {
    marginTop: 5,
    marginBottom: 5,
    width: 300,
    height: 50,
    borderWidth: 5,
    borderRadius: 20,
    color: '#000000',
    backgroundColor: '#FFFFFF',
    padding: 10
  },
  button: {
    width: 300,
    backgroundColor: 'red',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 12
  },
  buttonText: {
    fontSize: 16,
    color: '#FFFFFF',
    textAlign: 'center',
  },
  bawah: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  }
});
