import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Main from './Main'
import HomeStack from './HomeStack';

const Drawer = createDrawerNavigator();

const HomeDrawer = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name='home' component={HomeStack} />
      <Drawer.Screen name='main' component={Main} />
    </Drawer.Navigator>
  );
}

export default HomeDrawer;
