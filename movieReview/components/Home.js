import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Home = ({ navigation }) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        onPress={() => {navigation.push('main')}}
      >
        <Text>Go To Main Page</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Home;
