import React from 'react';
import {createStackNavigator} from '@react-navigation/stack' 
import Home from './Home'
import Main from './Main'

const Stack = createStackNavigator();
const HomeStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='home' component={Home} />
      <Stack.Screen name='main' component={Main} />
    </Stack.Navigator>
  );
}

export default HomeStack;
