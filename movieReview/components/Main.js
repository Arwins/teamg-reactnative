import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Main = ({ navigation }) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity
        onPress={() => {navigation.goBack()}}
      >
        <Text>Go To Home Page</Text>
      </TouchableOpacity>
    </View>
  );
}

export default Main;
