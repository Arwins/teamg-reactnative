import React from 'react';

import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import Stackers from './src/components/HomeStack';

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <Stackers />
    </NavigationContainer>
  );
};

export default App;
